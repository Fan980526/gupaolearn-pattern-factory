package com.fan.gupaolearn.pattern.factory.alibaba;

import com.fan.gupaolearn.pattern.factory.IRecharge;
import com.fan.gupaolearn.pattern.factory.IWithdraw;

public class AlibabaWithdraw implements IWithdraw {

    @Override
    public void withdraw() {
        System.out.println("支付宝提现发起。");
    }
}
