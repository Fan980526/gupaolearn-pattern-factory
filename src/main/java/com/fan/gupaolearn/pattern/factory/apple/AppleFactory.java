package com.fan.gupaolearn.pattern.factory.apple;

import com.fan.gupaolearn.pattern.factory.IRecharge;
import com.fan.gupaolearn.pattern.factory.IWithdraw;
import com.fan.gupaolearn.pattern.factory.PayFactory;

public class AppleFactory extends PayFactory {

    @Override
    public IRecharge getRecharge() {
        super.OtherInit();
        return new AppleRecharge();
    }

    @Override
    public IWithdraw getWithdraw() {
        super.OtherInit();
        return new AppleWithdraw();
    }
}
