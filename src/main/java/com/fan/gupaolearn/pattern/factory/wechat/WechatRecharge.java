package com.fan.gupaolearn.pattern.factory.wechat;

import com.fan.gupaolearn.pattern.factory.IRecharge;

public class WechatRecharge implements IRecharge {

    @Override
    public void recharge() {
        System.out.println("微信充值发起。");
    }
}
