package com.fan.gupaolearn.pattern.factory;

public abstract class PayFactory {

    public void ChinaInit(){
        System.out.println("国内初始化连接。");
    }

    public void OtherInit(){
        System.out.println("国外初始化连接。");
    }

    public abstract IRecharge getRecharge();

    public abstract IWithdraw getWithdraw();

}
