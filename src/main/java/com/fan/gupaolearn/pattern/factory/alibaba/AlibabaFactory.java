package com.fan.gupaolearn.pattern.factory.alibaba;

import com.fan.gupaolearn.pattern.factory.IRecharge;
import com.fan.gupaolearn.pattern.factory.IWithdraw;
import com.fan.gupaolearn.pattern.factory.PayFactory;

public class AlibabaFactory extends PayFactory {

    @Override
    public IRecharge getRecharge() {
        super.ChinaInit();
        return new AlibabaRecharge();
    }

    @Override
    public IWithdraw getWithdraw() {
        super.ChinaInit();
        return new AlibabaWithdraw();
    }
}
