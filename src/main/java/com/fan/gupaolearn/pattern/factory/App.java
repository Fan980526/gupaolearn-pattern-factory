package com.fan.gupaolearn.pattern.factory;

import com.fan.gupaolearn.pattern.factory.apple.AppleFactory;
import com.fan.gupaolearn.pattern.factory.wechat.WechatFactory;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        System.out.println("Hello World!");

        PayFactory pay = new WechatFactory();
        pay.getRecharge().recharge();
        pay.getWithdraw().withdraw();

        pay = new AppleFactory();
        pay.getRecharge().recharge();
        pay.getWithdraw().withdraw();


    }
}
