package com.fan.gupaolearn.pattern.factory.wechat;

import com.fan.gupaolearn.pattern.factory.IWithdraw;

public class WechatWithdraw implements IWithdraw {

    @Override
    public void withdraw() {
        System.out.println("微信提现发起。");
    }
}
