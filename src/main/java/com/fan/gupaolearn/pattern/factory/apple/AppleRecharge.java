package com.fan.gupaolearn.pattern.factory.apple;

import com.fan.gupaolearn.pattern.factory.IRecharge;

public class AppleRecharge implements IRecharge {

    @Override
    public void recharge() {
        System.out.println("支付宝充值发起。");
    }
}
