package com.fan.gupaolearn.pattern.factory.alibaba;

import com.fan.gupaolearn.pattern.factory.IRecharge;

public class AlibabaRecharge implements IRecharge {

    @Override
    public void recharge() {
        System.out.println("支付宝充值发起。");
    }
}
