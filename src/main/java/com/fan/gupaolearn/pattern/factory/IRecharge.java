package com.fan.gupaolearn.pattern.factory;

public interface IRecharge {

    void recharge();

}
