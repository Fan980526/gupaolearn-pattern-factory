package com.fan.gupaolearn.pattern.factory.apple;

import com.fan.gupaolearn.pattern.factory.IWithdraw;

public class AppleWithdraw implements IWithdraw {

    @Override
    public void withdraw() {
        System.out.println("支付宝提现发起。");
    }
}
