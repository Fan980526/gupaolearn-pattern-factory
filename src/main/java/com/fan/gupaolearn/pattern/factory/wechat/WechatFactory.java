package com.fan.gupaolearn.pattern.factory.wechat;

import com.fan.gupaolearn.pattern.factory.IRecharge;
import com.fan.gupaolearn.pattern.factory.IWithdraw;
import com.fan.gupaolearn.pattern.factory.PayFactory;

public class WechatFactory extends PayFactory {

    @Override
    public IRecharge getRecharge() {
        super.ChinaInit();
        return new WechatRecharge();
    }

    @Override
    public IWithdraw getWithdraw() {
        super.ChinaInit();
        return new WechatWithdraw();
    }
}
